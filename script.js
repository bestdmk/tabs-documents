$(document).ready(function () {
        /*Slider Tabs Documents */

        function mediaSize() {
          if (window.matchMedia("(max-width: 1200.98px)").matches) {
            $(".slider-leiloes .container").removeClass("container");
            $("#tabs-docs").slick({
              dots: false,
              infinite: false,
              speed: 300,
              slidesToShow: 1,
              slidesToScroll: 2,
              centerMode: true,
              arrows: false,
              variableWidth: true,
              autoplay: false,
              prevArrow: false,
              autoplaySpeed: 4000,
              responsive: [
                {
                  breakpoint: 1024,
                  settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1,
                  },
                },
                {
                  breakpoint: 600,
                  settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                  },
                },
                {
                  breakpoint: 480,
                  settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                  },
                },
              ],
            });
          } else {
          }
        }

        window.addEventListener("resize", mediaSize, true);
        mediaSize();

        $("#tabs-docs li:not(:first)").addClass("inactive");
        $(".tabs-docs-section .content").hide();
        $(".tabs-docs-section .nav-tabs-filter li:first").addClass("active");
        $(".tabs-docs-section .content:first").show();
        $("#tabs-docs li").click(function () {
          var t = $(this).attr("id");
          if ($(this).hasClass("inactive")) {
            $("#tabs-docs li").addClass("inactive");
            $("#tabs-docs li").removeClass("active");
            $(this).removeClass("inactive").addClass("active");
            $(".tabs-docs-section .content").hide();
            $("#" + t + "BB").fadeIn("slow");
          }
        });

        /* Fim Slider Tabs */
      });